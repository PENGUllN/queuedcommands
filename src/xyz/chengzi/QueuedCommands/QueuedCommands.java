package xyz.chengzi.QueuedCommands;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import xyz.chengzi.QueuedCommands.Updater.UpdateResult;
import xyz.chengzi.QueuedCommands.Updater.UpdateType;

import com.google.common.collect.Lists;

public class QueuedCommands extends JavaPlugin
{
    static final Map<UUID, List<String>> commandsMap = new HashMap<UUID, List<String>>();
    private static final Logger log = Bukkit.getLogger();
    
    @Override
    public void onEnable()
    {
        this.saveDefaultConfig();
        
        if (this.getConfig().getBoolean("enableMetrics", true))
            try
            {
                MetricsLite metrics = new MetricsLite(this);
                if (metrics.isOptOut())
                {
                    metrics = null;
                }
                else
                {
                    metrics.start();
                    log.info("Thank you for enabling Metrics!");
                }
            }
            catch (IOException e)
            {
                log.warning("Unable to start Metrics service.");
            }
        
        if (this.getConfig().getBoolean("enableUpdater", true))
        {
            final QueuedCommands plugin = this;
            Runnable updateChecker = new Runnable()
            {
                @Override
                public void run()
                {
                    Updater updater = new Updater(plugin, 82837, plugin.getFile(),
                            UpdateType.NO_DOWNLOAD, true);
                    if (updater.getResult() == UpdateResult.UPDATE_AVAILABLE)
                    {
                        log.info("New version " + updater.getLatestName().split(" ")[1]
                                + " for " + updater.getLatestGameVersion()
                                + " is available now!");
                    }
                }
            };
            Bukkit.getScheduler().runTaskAsynchronously(this, updateChecker);
        }
        
        ConfigurationSection playersSection = this.getConfig().getConfigurationSection(
                "Players");
        
        if (playersSection != null)
            for (String uuidString : playersSection.getKeys(false))
            {
                ConfigurationSection playerSection = playersSection
                        .getConfigurationSection(uuidString);
                commandsMap.put(UUID.fromString(uuidString),
                        playerSection.getStringList("commands"));
            }
        
        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);
    }
    
    @Override
    public void onDisable()
    {
        ConfigurationSection playersSection = this.getConfig().createSection("Players");
        
        for (Entry<UUID, List<String>> entry : commandsMap.entrySet())
        {
            ConfigurationSection playerSection = playersSection.createSection(entry
                    .getKey().toString());
            playerSection.set("commands", entry.getValue());
        }
        
        this.saveConfig();
    }
    
    @SuppressWarnings("deprecation")
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel,
            String[] args)
    {
        if (sender.hasPermission("QueuedCommands.admin"))
        {
            if (args.length >= 1)
            {
                switch (args[0].toLowerCase(Locale.ENGLISH))
                {
                    case "add":
                        if (args.length < 3)
                            return false;
                        OfflinePlayer p = Bukkit.getOfflinePlayer(args[1]);
                        if (p != null)
                        {
                            StringBuilder commandBuilder = new StringBuilder();
                            
                            for (int i = 2; i < args.length - 1; i++)
                            {
                                commandBuilder.append(args[i]);
                                commandBuilder.append(" ");
                            }
                            commandBuilder.append(args[args.length - 1]);
                            
                            if (p.isOnline()
                                    && !this.getConfig().getBoolean("onlyRunOnNextLogin",
                                            false))
                            {
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
                                        commandBuilder.toString());
                                sender.sendMessage(ChatColor.GREEN
                                        + "Player "
                                        + p.getName()
                                        + " is online, command will be processed instantly.");
                                return true;
                            }
                            
                            UUID uuid = p.getUniqueId();
                            List<String> commands = commandsMap.get(uuid);
                            if (commands == null)
                                commandsMap.put(uuid,
                                        Lists.newArrayList(commandBuilder.toString()));
                            else
                            {
                                commands.add(commandBuilder.toString());
                                commandsMap.put(uuid, commands);
                            }
                            sender.sendMessage(ChatColor.GREEN + "Command set to queue!");
                        }
                        else
                        {
                            sender.sendMessage(ChatColor.RED + "Cannot find player "
                                    + args[0] + ".");
                        }
                        break;
                    case "clear":
                        if (args.length != 2)
                            return false;
                        OfflinePlayer p2 = Bukkit.getOfflinePlayer(args[1]);
                        if (p2 != null)
                        {
                            commandsMap.remove(p2.getUniqueId());
                            sender.sendMessage(ChatColor.GREEN
                                    + "Commands removed from queue!");
                        }
                        else
                        {
                            sender.sendMessage(ChatColor.RED + "Cannot find player "
                                    + args[0] + ".");
                        }
                        break;
                    case "list":
                        if (args.length != 2)
                            return false;
                        OfflinePlayer p3 = Bukkit.getOfflinePlayer(args[1]);
                        if (p3 != null)
                        {
                            List<String> commands = commandsMap.get(p3.getUniqueId());
                            if (commands != null)
                            {
                                sender.sendMessage(ChatColor.GREEN
                                        + "Commands queued for player " + p3.getName()
                                        + ":");
                                for (String command : commands)
                                    sender.sendMessage("  " + command);
                            }
                            else
                            {
                                sender.sendMessage(ChatColor.RED
                                        + "No command was queued.");
                            }
                        }
                        else
                        {
                            sender.sendMessage(ChatColor.RED + "Cannot find player "
                                    + args[0] + ".");
                        }
                        break;
                    case "version":
                        if (args.length != 1)
                            return false;
                        sender.sendMessage(ChatColor.DARK_GREEN + "QueuedCommands v"
                                + this.getDescription().getVersion() + " by ChengZi368.");
                }
            }
            else
                return false;
        }
        else
            sender.sendMessage(ChatColor.RED
                    + "You don't have permission to use this command.");
        return true;
    }
}
